from django.contrib import admin
from django.urls import path
from shop.views import home_view, order_view, order_list_view, success_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view),
    path('order/<str:pizza_type>/<str:pizza_size>/', order_view),
    path('orders/', order_list_view),
    path('success/', success_view),
]
