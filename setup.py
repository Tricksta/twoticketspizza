from setuptools import setup

setup(
    name='TwoTicketsPizza',
    version='1.0',
    description='Coding Challenge for TwoTickets.de',
    author='Fabio Gentsch',
    author_email='f.gentsch98@gmail.com',
    classifiers=[
        'Programming Language :: Python :: 3.8',
        'Development Status :: 5 - Production/Stable',
        'Operating System :: OS Independent',
        'Intended Audience :: End Users/Desktop',
        'Natural Language :: English',
        'Environment :: Console',
    ],
    python_requires='>=3.8',
    install_requires=[
        'django==3.1.5',
    ],
)