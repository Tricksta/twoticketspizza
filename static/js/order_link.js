$(".order_link").click(function() {
    //get selected pizza size and type from the Select Element and then change the href of the order link according to it.
    let select = $(this).prev('select');
    let pizza_type = select.attr("id").split("_")[2];
    let pizza_size = select.children("option").filter(":selected").text();
    this.setAttribute("href", "/order/" + pizza_type + "/" + pizza_size + "/")
});
