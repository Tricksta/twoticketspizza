from django.shortcuts import render
from shop.models import Order
from django.db.models import Q

from django.core.validators import validate_email
from django.core.exceptions import ValidationError


def home_view(request):
    """Renders html template when function is called"""
    return render(request, "home.html")


def order_view(request, pizza_type, pizza_size):
    """
    Catch Post Data, validate it and then if valid, create a new Order object in the Database, else render
    template with an error msg
    """
    context = {"pizza_type": pizza_type, "pizza_size": pizza_size}

    if request.POST:
        client_name = request.POST.get("client_name")
        client_email = request.POST.get("client_email")
        client_phone = request.POST.get("client_phone")

        if client_name and client_email and client_phone:
            try:
                validate_email(client_email)
                Order(pizza_type=pizza_type, pizza_size=pizza_size[0], client_name=client_name,
                      client_email=client_email,
                      client_phone=client_phone).save()
                return render(request, "success.html", {"client_name": client_name})
            except ValidationError:
                context["error_msg"] = "Please enter a valid Email!"
        else:
            context["error_msg"] = "Plese fill all Fields!"

    return render(request, "order.html", context)


def order_list_view(request):
    """
    Catch Post Data,then create a set of filtered Order objects according to the given Filters, render List Template
    """
    context = {"orders": Order.objects.all()}

    if request.POST:
        client_name = request.POST.get("client_name")
        client_email = request.POST.get("client_email")

        if client_name and not client_email:
            context = {"orders": Order.objects.filter(client_name__contains=client_name)}
        elif not client_name and client_email:
            context = {"orders": Order.objects.filter(client_email__contains=client_email)}
        elif client_name and client_email:
            context = {"orders": Order.objects.filter(
                Q(client_name__contains=client_name) & Q(client_email__contains=client_email))}
        else:
            context = {"orders": Order.objects.all()}

    return render(request, "orderList.html", context)


def success_view(request):
    """Render success template, usually after an successful order"""
    return render(request, "success.html")
