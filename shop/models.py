from django.db import models


class Order(models.Model):
    pizza_sizes = [
        ("S", "Small"),
        ("M", "Medium"),
        ("L", "Large"),
    ]

    pizza_type = models.CharField(max_length=30)
    pizza_size = models.CharField(max_length=1, choices=pizza_sizes, default="M")
    client_name = models.CharField(max_length=70)
    client_email = models.CharField(max_length=255)  # RFC 3696 Email max length
    client_phone = models.CharField(max_length=15)  # ITU max Length for phone numbers
