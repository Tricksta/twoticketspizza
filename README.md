# TwoTicketsPizza

"Coding challenge for [TwoTickets](https://www.twotickets.de)"

How to Run the Project?
(CMD: means that u should execute the following comment in a shell or terminal)

1. Install [Python](https://www.python.org/downloads/) (min. V3.8)
2. Install Django with pip, CMD: "pip install django"
3. Clone or Download Project repo onto your pc
4. Go into the Django Project dir "/TwoTicketsPizza" where the manage.py is
5. CMD: python manage.py makemigrations
6. CMD: python manage.py migrate
7. CMD: python manage.py runserver
8. U can now vist the site @ http://127.0.0.1:8000/
